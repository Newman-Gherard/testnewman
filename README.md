# NEWMAN COMMANDS

#### RUNNING POSTMAN TESTS
newman run .\Petstore.json

#### NUMBER OF ITERATIONS (2)
newman run .\Petstore.json -n 2

#### POSTMAN TEST REPORT IN HTML
newman run .\Petstore.json -r htmlextra
